package com.example.tugas2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginPage extends AppCompatActivity {

    private EditText inputUsername, inputPassword;
    private Button button;
    private String username = "PutuGdeAryaBhanuartha";
    private String password = "215150400111037";

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page_layout);

        inputUsername = findViewById(R.id.etUsername);
        inputPassword = findViewById(R.id.etPassword);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String cleanedInputUsername = inputUsername.getText().toString().replaceAll("\\s", "");
                String cleanedPassword = inputPassword.getText().toString();
                if(cleanedInputUsername.equalsIgnoreCase(username) && cleanedPassword.equalsIgnoreCase(password)){
                    Intent login = new Intent(LoginPage.this, MainPage.class);
                    login.putExtra("username", inputUsername.getText().toString());
                    login.putExtra("password", password);
                    startActivity(login);
                    Toast.makeText(LoginPage.this, "Login Sukses", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(LoginPage.this, "Username atau password anda salah", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}