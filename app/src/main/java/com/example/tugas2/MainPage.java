package com.example.tugas2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page_layout);
        TextView textViewUsername = findViewById(R.id.inputUsername);
        TextView textViewPassword = findViewById(R.id.inputPassword);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String username = extras.getString("username");
            String password = extras.getString("password");
            textViewUsername.setText("Username: " + username);
            textViewPassword.setText("Password: " + password);
        }

    }
}